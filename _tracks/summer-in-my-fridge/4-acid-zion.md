---
layout: track
path: >-
  /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/summer-in-my-fridge/4-acid-zion.mp3
audio: /assets/albums/summer-in-my-fridge/4-acid-zion.mp3
slug: summer-in-my-fridge/4-acid-zion
albumSlug: summer-in-my-fridge
trackSlug: 4-acid-zion
coverPath: >-
  /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/summer-in-my-fridge/4-acid-zion.jpeg
cover: /assets/albums/summer-in-my-fridge/4-acid-zion.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 16119936
  duration: 365.5314285714286
native:
  ID3v2.3:
    - id: TIT2
      value: Acid Zion
    - id: TPE1
      value: Sethy Bowoy
    - id: TALB
      value: Summer In My Fridge
    - id: TRCK
      value: '4'
    - id: TCON
      value: Acid House
    - id: TYER
      value: '2020'
    - id: TDAT
      value: '2207'
quality:
  warnings: []
common:
  track:
    'no': 4
    of: null
  disk:
    'no': null
    of: null
  title: Acid Zion
  artists:
    - Sethy Bowoy
  artist: Sethy Bowoy
  album: Summer In My Fridge
  genre:
    - Acid House
  year: 2020
transformed:
  ID3v2.3:
    TIT2: Acid Zion
    TPE1: Sethy Bowoy
    TALB: Summer In My Fridge
    TRCK: '4'
    TCON: Acid House
    TYER: '2020'
    TDAT: '2207'
all:
  TIT2: Acid Zion
  TPE1: Sethy Bowoy
  TALB: Summer In My Fridge
  TRCK: '4'
  TCON: Acid House
  TYER: '2020'
  TDAT: '2207'
nextTrack:
  path: >-
    /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/summer-in-my-fridge/1-acid-bench.mp3
  audio: /assets/albums/summer-in-my-fridge/1-acid-bench.mp3
  slug: summer-in-my-fridge/1-acid-bench
  albumSlug: summer-in-my-fridge
  trackSlug: 1-acid-bench
  coverPath: >-
    /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/summer-in-my-fridge/1-acid-bench.jpeg
  cover: /assets/albums/summer-in-my-fridge/1-acid-bench.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 192000
    codecProfile: CBR
    numberOfSamples: 15001344
    duration: 340.1665306122449
  native:
    ID3v2.3:
      - id: TIT2
        value: Acid Bench
      - id: TPE1
        value: Sethy Bowoy
      - id: TALB
        value: Summer In My Fridge
      - id: TRCK
        value: '1'
      - id: TCON
        value: Acid House
      - id: TYER
        value: '2020'
      - id: TDAT
        value: '0707'
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    title: Acid Bench
    artists:
      - Sethy Bowoy
    artist: Sethy Bowoy
    album: Summer In My Fridge
    genre:
      - Acid House
    year: 2020
  transformed:
    ID3v2.3:
      TIT2: Acid Bench
      TPE1: Sethy Bowoy
      TALB: Summer In My Fridge
      TRCK: '1'
      TCON: Acid House
      TYER: '2020'
      TDAT: '0707'
  all:
    TIT2: Acid Bench
    TPE1: Sethy Bowoy
    TALB: Summer In My Fridge
    TRCK: '1'
    TCON: Acid House
    TYER: '2020'
    TDAT: '0707'
previousTrack:
  path: >-
    /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/summer-in-my-fridge/3-acid-alert.mp3
  audio: /assets/albums/summer-in-my-fridge/3-acid-alert.mp3
  slug: summer-in-my-fridge/3-acid-alert
  albumSlug: summer-in-my-fridge
  trackSlug: 3-acid-alert
  coverPath: >-
    /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/summer-in-my-fridge/3-acid-alert.jpeg
  cover: /assets/albums/summer-in-my-fridge/3-acid-alert.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 192000
    codecProfile: CBR
    numberOfSamples: 17335296
    duration: 393.09061224489795
  native:
    ID3v2.3:
      - id: TIT2
        value: Acid Alert
      - id: TPE1
        value: Sethy Bowoy
      - id: TALB
        value: Summer In My Fridge
      - id: TRCK
        value: '3'
      - id: TCON
        value: Acid House
      - id: TYER
        value: '2020'
      - id: TDAT
        value: '1607'
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    title: Acid Alert
    artists:
      - Sethy Bowoy
    artist: Sethy Bowoy
    album: Summer In My Fridge
    genre:
      - Acid House
    year: 2020
  transformed:
    ID3v2.3:
      TIT2: Acid Alert
      TPE1: Sethy Bowoy
      TALB: Summer In My Fridge
      TRCK: '3'
      TCON: Acid House
      TYER: '2020'
      TDAT: '1607'
  all:
    TIT2: Acid Alert
    TPE1: Sethy Bowoy
    TALB: Summer In My Fridge
    TRCK: '3'
    TCON: Acid House
    TYER: '2020'
    TDAT: '1607'
---
