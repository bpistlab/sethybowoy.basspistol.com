---
layout: track
path: >-
  /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/naranja-green/1-naranja-green.mp3
audio: /assets/albums/naranja-green/1-naranja-green.mp3
slug: naranja-green/1-naranja-green
albumSlug: naranja-green
trackSlug: 1-naranja-green
coverPath: >-
  /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/naranja-green/1-naranja-green.jpeg
cover: /assets/albums/naranja-green/1-naranja-green.jpeg
format: &ref_0
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 96000
  codecProfile: CBR
  numberOfSamples: 15593472
  duration: 353.5934693877551
native: &ref_1
  ID3v2.3:
    - id: TIT2
      value: Naranja Green
    - id: TPE1
      value: Sethy Bowoy
    - id: TRCK
      value: '1'
    - id: TCON
      value: Mellow 2step
    - id: TALB
      value: Naranja Green
    - id: TYER
      value: '2012'
quality: &ref_2
  warnings: []
common: &ref_3
  track:
    'no': 1
    of: null
  disk:
    'no': null
    of: null
  title: Naranja Green
  artists:
    - Sethy Bowoy
  artist: Sethy Bowoy
  genre:
    - Mellow 2step
  album: Naranja Green
  year: 2012
transformed: &ref_4
  ID3v2.3:
    TIT2: Naranja Green
    TPE1: Sethy Bowoy
    TRCK: '1'
    TCON: Mellow 2step
    TALB: Naranja Green
    TYER: '2012'
all: &ref_5
  TIT2: Naranja Green
  TPE1: Sethy Bowoy
  TRCK: '1'
  TCON: Mellow 2step
  TALB: Naranja Green
  TYER: '2012'
nextTrack: &ref_6
  path: >-
    /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/naranja-green/1-naranja-green.mp3
  audio: /assets/albums/naranja-green/1-naranja-green.mp3
  slug: naranja-green/1-naranja-green
  albumSlug: naranja-green
  trackSlug: 1-naranja-green
  coverPath: >-
    /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/naranja-green/1-naranja-green.jpeg
  cover: /assets/albums/naranja-green/1-naranja-green.jpeg
  format: *ref_0
  native: *ref_1
  quality: *ref_2
  common: *ref_3
  transformed: *ref_4
  all: *ref_5
previousTrack: *ref_6
---
