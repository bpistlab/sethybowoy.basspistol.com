---
layout: track
path: >-
  /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/inflating-saturn/1-commersens-temperatur.mp3
audio: /assets/albums/inflating-saturn/1-commersens-temperatur.mp3
slug: inflating-saturn/1-commersens-temperatur
albumSlug: inflating-saturn
trackSlug: 1-commersens-temperatur
coverPath: >-
  /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/inflating-saturn/1-commersens-temperatur.jpeg
cover: /assets/albums/inflating-saturn/1-commersens-temperatur.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.99r
  duration: 220.55183673469386
native:
  ID3v2.3:
    - id: TIT2
      value: Commersens Temperatur
    - id: TPE1
      value: Sethy Bowoy
    - id: TALB
      value: Inflating Saturn
    - id: TRCK
      value: '1'
    - id: TCON
      value: '35'
    - id: TYER
      value: '2014'
quality:
  warnings: []
common:
  track:
    'no': 1
    of: null
  disk:
    'no': null
    of: null
  title: Commersens Temperatur
  artists:
    - Sethy Bowoy
  artist: Sethy Bowoy
  album: Inflating Saturn
  genre:
    - House
  year: 2014
transformed:
  ID3v2.3:
    TIT2: Commersens Temperatur
    TPE1: Sethy Bowoy
    TALB: Inflating Saturn
    TRCK: '1'
    TCON: '35'
    TYER: '2014'
all:
  TIT2: Commersens Temperatur
  TPE1: Sethy Bowoy
  TALB: Inflating Saturn
  TRCK: '1'
  TCON: '35'
  TYER: '2014'
nextTrack: &ref_0
  path: >-
    /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/inflating-saturn/2-saturn-day.mp3
  audio: /assets/albums/inflating-saturn/2-saturn-day.mp3
  slug: inflating-saturn/2-saturn-day
  albumSlug: inflating-saturn
  trackSlug: 2-saturn-day
  coverPath: >-
    /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/inflating-saturn/2-saturn-day.jpeg
  cover: /assets/albums/inflating-saturn/2-saturn-day.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.99r
    duration: 234.42285714285714
  native:
    ID3v2.3:
      - id: TIT2
        value: Saturn Day
      - id: TPE1
        value: Sethy Bowoy
      - id: TALB
        value: Inflating Saturn
      - id: TRCK
        value: '2'
      - id: TCON
        value: '35'
      - id: TYER
        value: '2014'
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    title: Saturn Day
    artists:
      - Sethy Bowoy
    artist: Sethy Bowoy
    album: Inflating Saturn
    genre:
      - House
    year: 2014
  transformed:
    ID3v2.3:
      TIT2: Saturn Day
      TPE1: Sethy Bowoy
      TALB: Inflating Saturn
      TRCK: '2'
      TCON: '35'
      TYER: '2014'
  all:
    TIT2: Saturn Day
    TPE1: Sethy Bowoy
    TALB: Inflating Saturn
    TRCK: '2'
    TCON: '35'
    TYER: '2014'
previousTrack: *ref_0
---
