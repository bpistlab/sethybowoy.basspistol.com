---
layout: track
path: >-
  /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/phramtiden-syrliga-sp-r/1-phramtiden-syrliga-sp-r.mp3
audio: /assets/albums/phramtiden-syrliga-sp-r/1-phramtiden-syrliga-sp-r.mp3
slug: phramtiden-syrliga-sp-r/1-phramtiden-syrliga-sp-r
albumSlug: phramtiden-syrliga-sp-r
trackSlug: 1-phramtiden-syrliga-sp-r
coverPath: >-
  /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/phramtiden-syrliga-sp-r/1-phramtiden-syrliga-sp-r.jpeg
cover: /assets/albums/phramtiden-syrliga-sp-r/1-phramtiden-syrliga-sp-r.jpeg
format: &ref_0
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.99r
  duration: 307.8791836734694
native: &ref_1
  ID3v2.3:
    - id: TALB
      value: Phramtiden Syrliga Spår
    - id: TRCK
      value: '1'
    - id: TCON
      value: '34'
    - id: TIT2
      value: Phramtiden Syrliga Spår
    - id: TPE1
      value: Sethy Bowoy
    - id: TYER
      value: '2014'
quality: &ref_2
  warnings: []
common: &ref_3
  track:
    'no': 1
    of: null
  disk:
    'no': null
    of: null
  album: Phramtiden Syrliga Spår
  genre:
    - Acid
  title: Phramtiden Syrliga Spår
  artists:
    - Sethy Bowoy
  artist: Sethy Bowoy
  year: 2014
transformed: &ref_4
  ID3v2.3:
    TALB: Phramtiden Syrliga Spår
    TRCK: '1'
    TCON: '34'
    TIT2: Phramtiden Syrliga Spår
    TPE1: Sethy Bowoy
    TYER: '2014'
all: &ref_5
  TALB: Phramtiden Syrliga Spår
  TRCK: '1'
  TCON: '34'
  TIT2: Phramtiden Syrliga Spår
  TPE1: Sethy Bowoy
  TYER: '2014'
nextTrack: &ref_6
  path: >-
    /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/phramtiden-syrliga-sp-r/1-phramtiden-syrliga-sp-r.mp3
  audio: /assets/albums/phramtiden-syrliga-sp-r/1-phramtiden-syrliga-sp-r.mp3
  slug: phramtiden-syrliga-sp-r/1-phramtiden-syrliga-sp-r
  albumSlug: phramtiden-syrliga-sp-r
  trackSlug: 1-phramtiden-syrliga-sp-r
  coverPath: >-
    /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/phramtiden-syrliga-sp-r/1-phramtiden-syrliga-sp-r.jpeg
  cover: /assets/albums/phramtiden-syrliga-sp-r/1-phramtiden-syrliga-sp-r.jpeg
  format: *ref_0
  native: *ref_1
  quality: *ref_2
  common: *ref_3
  transformed: *ref_4
  all: *ref_5
previousTrack: *ref_6
---
