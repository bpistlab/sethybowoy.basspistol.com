---
layout: track
path: >-
  /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/soulgap/1-soulgap.mp3
audio: /assets/albums/soulgap/1-soulgap.mp3
slug: soulgap/1-soulgap
albumSlug: soulgap
trackSlug: 1-soulgap
coverPath: >-
  /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/soulgap/1-soulgap.jpeg
cover: /assets/albums/soulgap/1-soulgap.jpeg
format: &ref_0
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 96000
  codecProfile: CBR
  numberOfSamples: 15805440
  duration: 358.4
native: &ref_1
  ID3v2.3:
    - id: TIT2
      value: Soulgap
    - id: TPE1
      value: Sethy Bowoy
    - id: TALB
      value: Soulgap
    - id: TCON
      value: Jazzy House
    - id: TRCK
      value: '1'
    - id: TYER
      value: '2011'
quality: &ref_2
  warnings: []
common: &ref_3
  track:
    'no': 1
    of: null
  disk:
    'no': null
    of: null
  title: Soulgap
  artists:
    - Sethy Bowoy
  artist: Sethy Bowoy
  album: Soulgap
  genre:
    - Jazzy House
  year: 2011
transformed: &ref_4
  ID3v2.3:
    TIT2: Soulgap
    TPE1: Sethy Bowoy
    TALB: Soulgap
    TCON: Jazzy House
    TRCK: '1'
    TYER: '2011'
all: &ref_5
  TIT2: Soulgap
  TPE1: Sethy Bowoy
  TALB: Soulgap
  TCON: Jazzy House
  TRCK: '1'
  TYER: '2011'
nextTrack: &ref_6
  path: >-
    /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/soulgap/1-soulgap.mp3
  audio: /assets/albums/soulgap/1-soulgap.mp3
  slug: soulgap/1-soulgap
  albumSlug: soulgap
  trackSlug: 1-soulgap
  coverPath: >-
    /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/soulgap/1-soulgap.jpeg
  cover: /assets/albums/soulgap/1-soulgap.jpeg
  format: *ref_0
  native: *ref_1
  quality: *ref_2
  common: *ref_3
  transformed: *ref_4
  all: *ref_5
previousTrack: *ref_6
---
