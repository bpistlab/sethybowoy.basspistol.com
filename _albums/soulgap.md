---
layout: album
slug: soulgap
name: Soulgap
artists: Sethy Bowoy
bitrate: 96000
trackCount: 1
cover: /assets/albums/soulgap/1-soulgap.jpeg
date: 2011-1-1
tracks:
  - path: >-
      /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/soulgap/1-soulgap.mp3
    audio: /assets/albums/soulgap/1-soulgap.mp3
    slug: soulgap/1-soulgap
    albumSlug: soulgap
    trackSlug: 1-soulgap
    coverPath: >-
      /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/soulgap/1-soulgap.jpeg
    cover: /assets/albums/soulgap/1-soulgap.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 96000
      codecProfile: CBR
      numberOfSamples: 15805440
      duration: 358.4
    native:
      ID3v2.3:
        - id: TIT2
          value: Soulgap
        - id: TPE1
          value: Sethy Bowoy
        - id: TALB
          value: Soulgap
        - id: TCON
          value: Jazzy House
        - id: TRCK
          value: '1'
        - id: TYER
          value: '2011'
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      title: Soulgap
      artists:
        - Sethy Bowoy
      artist: Sethy Bowoy
      album: Soulgap
      genre:
        - Jazzy House
      year: 2011
    transformed:
      ID3v2.3:
        TIT2: Soulgap
        TPE1: Sethy Bowoy
        TALB: Soulgap
        TCON: Jazzy House
        TRCK: '1'
        TYER: '2011'
    all:
      TIT2: Soulgap
      TPE1: Sethy Bowoy
      TALB: Soulgap
      TCON: Jazzy House
      TRCK: '1'
      TYER: '2011'
---
