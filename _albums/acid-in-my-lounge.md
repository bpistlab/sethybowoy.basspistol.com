---
layout: album
slug: acid-in-my-lounge
name: Acid In My Lounge
artists: Sethy Bowoy
bitrate: 320000
trackCount: 1
cover: /assets/albums/acid-in-my-lounge/1-acid-in-my-lounge.jpeg
date: 2013-1-1
tracks:
  - path: >-
      /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/acid-in-my-lounge/1-acid-in-my-lounge.mp3
    audio: /assets/albums/acid-in-my-lounge/1-acid-in-my-lounge.mp3
    slug: acid-in-my-lounge/1-acid-in-my-lounge
    albumSlug: acid-in-my-lounge
    trackSlug: 1-acid-in-my-lounge
    coverPath: >-
      /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/acid-in-my-lounge/1-acid-in-my-lounge.jpeg
    cover: /assets/albums/acid-in-my-lounge/1-acid-in-my-lounge.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.99r
      duration: 216.08489795918368
    native:
      ID3v2.3:
        - id: TPE1
          value: Sethy Bowoy
        - id: TIT2
          value: Acid In My Lounge
        - id: TALB
          value: Acid In My Lounge
        - id: TRCK
          value: '1'
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2013'
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      artists:
        - Sethy Bowoy
      artist: Sethy Bowoy
      title: Acid In My Lounge
      album: Acid In My Lounge
      genre:
        - Electronic
      year: 2013
    transformed:
      ID3v2.3:
        TPE1: Sethy Bowoy
        TIT2: Acid In My Lounge
        TALB: Acid In My Lounge
        TRCK: '1'
        TCON: Electronic
        TYER: '2013'
    all:
      TPE1: Sethy Bowoy
      TIT2: Acid In My Lounge
      TALB: Acid In My Lounge
      TRCK: '1'
      TCON: Electronic
      TYER: '2013'
---
