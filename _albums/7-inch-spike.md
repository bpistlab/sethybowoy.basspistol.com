---
layout: album
slug: 7-inch-spike
name: 7 Inch Spike
artists: Sethy Bowoy
bitrate: 320000
trackCount: 1
cover: /assets/albums/7-inch-spike/1-7-inch-spike.jpeg
date: 2013-1-1
tracks:
  - path: >-
      /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/7-inch-spike/1-7-inch-spike.mp3
    audio: /assets/albums/7-inch-spike/1-7-inch-spike.mp3
    slug: 7-inch-spike/1-7-inch-spike
    albumSlug: 7-inch-spike
    trackSlug: 1-7-inch-spike
    coverPath: >-
      /home/set/git/basspistol/sethybowoy.basspistol.com/assets/albums/7-inch-spike/1-7-inch-spike.jpeg
    cover: /assets/albums/7-inch-spike/1-7-inch-spike.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.99r
      duration: 352.23510204081634
    native:
      ID3v2.3:
        - id: TIT2
          value: 7 Inch Spike
        - id: TPE1
          value: Sethy Bowoy
        - id: TALB
          value: 7 Inch Spike
        - id: TRCK
          value: '1'
        - id: TCON
          value: '34'
        - id: TYER
          value: '2013'
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      title: 7 Inch Spike
      artists:
        - Sethy Bowoy
      artist: Sethy Bowoy
      album: 7 Inch Spike
      genre:
        - Acid
      year: 2013
    transformed:
      ID3v2.3:
        TIT2: 7 Inch Spike
        TPE1: Sethy Bowoy
        TALB: 7 Inch Spike
        TRCK: '1'
        TCON: '34'
        TYER: '2013'
    all:
      TIT2: 7 Inch Spike
      TPE1: Sethy Bowoy
      TALB: 7 Inch Spike
      TRCK: '1'
      TCON: '34'
      TYER: '2013'
---
